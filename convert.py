#!/usr/bin/env python
# coding: utf-8

import rdflib, csv, sys
from rdflib.namespace import CSVW, DC, DCAT, DCTERMS, DOAP, FOAF, ODRL2, ORG, OWL, PROF, PROV, RDF, RDFS, SDO, SH, SKOS, SOSA, SSN, TIME, VOID, XMLNS, XSD

numArg = len(sys.argv) - 1
if numArg != 2:
    print('Incorrect number of arguments provided!')
    print("Run as './convert.py inputFile outputFile'")
    sys.exit(1)

filename = sys.argv[1] #'./Taverna_repository/workflow_2181_version_3/run_2/workflowrun.prov.ttl'

g = rdflib.Graph()
g.load(filename, format=rdflib.util.guess_format(filename))
print('Loaded Dataset with %s triples.' % len(g))
print('Discovered %s explicitly typed PROV Activities.' % len(list(g.subjects(RDF.type, PROV.Activity))))



print('Entailing types through PROV RDFS semantics ...')
# Load PROV Ontology Triples ...
g.load('http://www.w3.org/ns/prov-o')

# ... and infer potentially missing Provenance information in the datset
from owlrl import DeductiveClosure, RDFS_Semantics
DeductiveClosure(RDFS_Semantics).expand(g) # Only RDFS Semantics - this might not be enough
#from owlrl import OWLRL_Extension
#DeductiveClosure(OWLRL_Semantics).expand(graph) # calculate an OWL 2 RL deductive closure of graph without axiomatic triples
#DeductiveClosure(OWLRL_Extension, rdfs_closure = True, axiomatic_triples = True, datatype_axioms = True).expand(graph)
#from owlrl import CombinedClosure.RDFS_OWLRL_Semantics
#DeductiveClosure(RDFS_OWLRL_Semantics)

print('There are now %s PROV Activities and %s triples total.' % (len(list(g.subjects(RDF.type, PROV.Activity))), len(g)))



# data Properties
qres = g.query("""
SELECT DISTINCT ?p {
    ?event a <http://www.w3.org/ns/prov#Activity>;
        ?p ?o.
    FILTER isLiteral(?o).
}""")

# import re
# # exclude PROV, WFPROV and RDF properties
# ex = re.compile('(http://www.w3.org/ns/prov#|http://purl.org/wf4ever/wfprov#)')
# qres = filter(lambda row: not ex.match("%s" % row), qres)

dataProperties = []
for row in qres:
    dataProperties.append(row['p'])
    
print('Discovered %s data properties to consider for conversion:' % len(dataProperties)) 
print(sorted(map(lambda p: '%s' % p, dataProperties)))



# object Properties
qres = g.query("""
SELECT DISTINCT ?p {
    ?event a <http://www.w3.org/ns/prov#Activity>;
        ?p ?o.
    FILTER isIRI(?o).
}""")

# # exclude PROV, WFPROV and RDF properties
# ex = re.compile('(http://www.w3.org/ns/prov#|http://purl.org/wf4ever/wfprov#|http://www.w3.org/1999/02/22-rdf-syntax-ns#)')
# qres = filter(lambda row: not ex.match("%s" % row), qres)

objectProperties = [] # [PROV.used]
for row in qres: 
    objectProperties.append(row['p'])

print('Discovered %s object properties to consider for conversion:' % len(objectProperties)) 
print(sorted(map(lambda p: '%s' % p, objectProperties)))



# Setting up colums for the CSV export
cols = ['eventID', 'activityType', 'startTime', 'endTime', 'objects2right']
for p in objectProperties:
    if p not in [RDF.type]:
        cols.append('%s' % p)
cols.append('attributes2right')
for p in dataProperties:
    if p not in [PROV.endedAtTime, PROV.startedAtTime]:
        cols.append('%s' % p)



print('Converting to PROV Trace to Object-Centric Process Log...')
with open(sys.argv[2], 'w', newline='') as csvfile:
    # create csv writer
    writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow(cols)
    
    # iterate events
    for a in g.subjects(RDF.type, PROV.Activity):

        # get all data of event
        eventData = g[a:]

        # prepare empty row to be added
        row = dict(zip(cols, [[] for i in range(len(cols))]))
        for k in ['startTime','endTime','objects2right','attributes2right']:
            row[k] = None

        row['eventID'] = '%s' % a

        # add event data to row
        for p, o in eventData:
            if type(o) != rdflib.term.BNode:
                if p == RDF.type:
                    if o not in [OWL.Thing, PROV.Activity, RDFS.Resource]:
                        row['activityType'].append('%s' % o)
                elif p == PROV.startedAtTime:
                    row['startTime'] = '%s' % o
                elif p == PROV.endedAtTime:
                    row['endTime'] = '%s' % o
                else:
                    row['%s' % p].append('%s' % o)

        writer.writerow([row[k] for k in cols])
print('Finished conversion successfully!')

