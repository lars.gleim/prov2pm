# PROV2PM

A small script to convert PROV traces to Object-centric Process Logs, usable for subsequent process mining.

### Installation 

Install dependencies: `pip install rdflib, owlrl, requests`

### PROV Source Data

Find existing traces e.g. at https://github.com/wf4ever/provenance-corpus or generate them e.g. from GIT repositories using https://github.com/IDLabResearch/Git2PROV 

### Usage

Convert PROV traces to Object-centric Process Log via `python convert.py inputFile.ttl outputFile.csv`

### Output Format

The conversion results in CSV files similar to the following:

| eventID                                  | activityType                                   | startTime                        | endTime                          | objects2right | http://www.w3.org/ns/prov#used        | … | attributes2right | http://www.w3.org/2000/01/rdf-schema#label                                              | … |
|------------------------------------------|------------------------------------------------|----------------------------------|----------------------------------|---------------|---------------------------------------|---|------------------|-----------------------------------------------------------------------------------------|---|
| http://ex.org/run/97dc1a/                | ['http://purl.org/wf4ever/wfprov#WorkflowRun'] | 2012-09-12T16:40:03.912000+01:00 | 2012-09-12T16:43:08.126000+01:00 |               | ['http://ex.org/6eaf1a//8876fc']      |   |                  | ['Workflow run of LipidMaps_Query']                                                     |   |
| http://ex.org/run/97dc1a/process/76ca8e/ | []                                             | 2012-09-12T16:41:08.081000+01:00 | 2012-09-12T16:41:09.868000+01:00 |               | ['http://ex.org/7a1bef/7945b8']       |   |                  | ['Processor execution split_whitespaces (facade613:LipidMaps_Query:split_whitespaces)'] |   |
| http://ex.org/run/97dc1a/process/0eb618/ | []                                             | 2012-09-12T16:42:51.236000+01:00 | 2012-09-12T16:42:51.288000+01:00 |               | ['http://ex.org/list/36697c/false/1'] |   |                  | ['Processor execution split_entries (facade613:LipidMaps_Query:split_entries)']         |   |
| …                                        | …                                              | …                                | …                                | …             | …                                     | … | …                | …                                                                                       | … |


The columns `eventID`, `activityType`, `startTime`, `endTime`, `objects2right` and `attributes2right` have fixed names.
The columns `eventID`, `startTime` and `endTime` are "normal" string values.
The columns `objects2right` and `attributes2right` are always left blank deliberately to serve as delimiter columns. As their names indicate, object properties can be found in the columns to the right of `objects2right` and data properties to the right of `attributes2right`.
All other columns are of list (i.e. array) type to capture the variable number of related objects and values.
